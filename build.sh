#!/bin/bash

echo "Checking the potion cauldron for all ingredients"
if ! command -v packagesbuild &> /dev/null
then
    echo "packagesbuild could not be found"
    echo "Download from http://s.sudre.free.fr/Software/Packages/about.html"
    sleep 1
    open http://s.sudre.free.fr/Software/Packages/about.html
    exit
fi


if ! command -v dmgbuild &> /dev/null
then
    echo "dmgbuild could not be found, but I can install it if you have pip!"
    if command -v pip3 &> /dev/null
        then 
           pip3 install dmgbuild
        else
           echo "Saddly there is no pip3, install python all human like"
	   echo "if you have brew:  brew install python" 
           exit
    fi
fi


echo "Clean Build directory"
rm -rfv build/*

echo "Remove old Read Me"
rm -rfv Preconfigure/MenuItems/Support.viscositymenuitem/Read\ Me.rtf*

echo "  \-- Copy in the new Read Me"
cp -rv Custom\ Assets/Docs/Read\ Me.rtf* Preconfigure/MenuItems/Support.viscositymenuitem/

echo "Remove old Watch Me"
rm -rfv Preconfigure/MenuItems/Support.viscositymenuitem/Watch\ Me.???

echo "  \-- Copy in the new Watch Me"
cp -v Custom\ Assets/Video/Watch\ Me.??? Preconfigure/MenuItems/Support.viscositymenuitem/

echo "Building Installer"
/usr/local/bin/packagesbuild Brainium\ VPN\ Installer.pkgproj

echo "Sign the installer"
productsign --sign "Apple Distribution: Brainium Studios LLC (7S85E2DAW8)" "build/VPN Installer unsigned.pkg" "build/VPN Installer.pkg"
if [[ "$?" == 0 ]]; then
	echo "Do you want to do Notarizing? (needs exactly 'yes' to do so)"
	read Notarizing

	if [[ "$Notarizing" == "yes" ]]; then
		xcrun_password="$(cat .password)"
		echo "xcrun_password  is  ${xcrun_password}"
		xcrun altool --notarize-app \
		  --primary-bundle-id "com.sparklabs.pkg.ViscosityInstaller" \
		  --username "citizen.kepler@brainium.com" \
		  --password "$xcrun_password"\
		  --file "build/VPN Installer.pkg"


		while true; do
			echo "wait for the email to staple the Notarizing"
			read $read
			echo "Lets go"
			xcrun stapler staple "build/VPN Installer.pkg"
			value="$?"
			echo $value
			if [[ "$value" -eq "0" ]]; then echo "yay";break; fi
		done


	else
		echo "No Notarizing"
	fi
else 
	echo "Signing Failed"
	cp -v "build/VPN Installer unsigned.pkg" "build/VPN Installer.pkg"
fi

echo "Building DNG file"
dmgbuild -s Custom\ Assets/DNG\ Assets/configuration.json "Brainium VPN Installer" build/Brainium\ VPN\ Installer

echo "Open the final product"
open build/Brainium\ VPN\ Installer.dmg
