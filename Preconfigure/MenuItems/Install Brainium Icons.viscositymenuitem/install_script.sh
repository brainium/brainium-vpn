#!/bin/bash
echo "Installing Menu Icons"
sudo cp -r ~/Library/'Application Support'/Viscosity/MenuItems/'Install Brainium Icons'.viscositymenuitem/MenuIcons/* /Applications/'Viscosity.app'/Contents/Resources/'Menu Icons'/
echo " - Menu Icons Installed"
sudo rm -rf ~/Library/'Application Support'/Viscosity/MenuItems/'Install Brainium Icons'.viscositymenuitem &> /dev/null
echo " - Removed Menu Icon Installer option"
sudo pkill Viscosity
echo " - Closed Brainium VPN Client"
defaults write com.viscosityvpn.Viscosity MenuBarIcons -string "BrainiumColored"
echo " - Set Menu Icon to BrainiumColored"
open /Applications/Viscosity.app/
echo " - Launched Brainium VPN Client"
echo "----------------------------"
echo "Install is complete.  It's safe to close this window"
